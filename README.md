OlympicGames
---------

O objetivo da aplicação OlympicGames, é auxiliar na gestão de dados das competições dos Jogos Olímpicos Tokyo 2020. Para esse fim, foram criados dois endpoints para cadastro das competições e consultas.

O desenvolvimento dessa aplicação foi realizado através da linguagem de programação Java, e o auxilio da IDE Netbeans. Para gerir as dependências de pacotes e frameworks, utilizou-se o Maven. O mapeamento de entidades e persistência dos dados foi realizado com o framework JPA, e para manipulação dos dados persistidos foi utilizado o HSQL, onde armazenou-se os dados em memória.

Durante o desenvolvimento desta aplicação, foi necessário a manipulação de datas, para auxiliar tais manipulações utilizou-se o JodaTime. Inicialmente tinha-se como ideia utilizar o LocalDate e LocalDateTime, nativos a partir da versão 8 do java, porém no decorrer do desenvolvimento houve conflitos entre tais Api's e a IDE Netbeans. Para contornar tais conflitos e concluir o desenvolvimento dentro do prazo, optou-se por utilizar o JodaTime.

As respostas dos endpoints foram trabalhadas utilizando o Gson, para facilitar e aumentar o desempenho do desenvolvimento.

Criou-se testes para os fluxos da aplicação, utilizando o Junit, onde foi possível testar os principais métodos da aplicação. Por se tratar de uma aplicação simples, não foi utilizado plataformas que auxiliam nos testes, por exemplo, como o Arquilian, onde poderia-se automatizar testes e injeções de dependências.

Foi utilizado o Glassfish como servidor da aplicação, na versão 4.

Tal aplicação estará disponível através do link abaixo:
https://bitbucket.org/filipi02/olympicgames

### Endpoints
Para utilizar o endpoint de cadastro de competições utiliza-se a seguinte URL: #{caminhoDaAplicacao}/olympicgames/game utilizando o método POST e o cabeçalho Content-Type: application/json, e no corpo da requisição informar os dados a serem persistido, conforme modelo abaixo:


```java
{ 
  "firstCountry" :  "Brasil",
  "secondCountry" : "Alemanha",
  "modality" : "Futebol",
  "local" : "Mineirão",
  "initDate" : "2017-09-13 22:10:00",
  "endDate" : "2017-09-13 22:10:00",
  "round" : "Eliminatórias"
}
```

Na raiz da aplicação, encontra-se uma imagem intitulada post.png ilustrando tal requisição.

Para utilizar o endpoint de listagem de competições utiliza-se a seguinte URL: #{caminhoDaAplicacao}/olympicgames/game utilizando o método GET, onde o retorno será uma lista com todas as competições criadas através da aplicação, com ordenação de data. 

Na raiz da aplicação, encontra-se uma imagem intitulada get.png ilustrando tal requisição.

É possível listar as competições por determinada modalidade. Para isso utiliza-se a seguinte URL: #{caminhoDaAplicacao}/olympicgames/game/{modalidade} utilizando o método GET.

Na raiz da aplicação, encontra-se uma imagem intitulada getModalidade.png ilustrando tal requisição.

### Contato
Para maiores informações filipi.nicolau@gmail.com
