package game;

/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
import br.com.olympicgames.model.GameDTO;
import br.com.olympicgames.service.GameService;
import br.com.olympicgames.util.FieldInvalidException;
import br.com.olympicgames.util.MaxGamesException;
import br.com.olympicgames.util.Message;
import br.com.olympicgames.util.ObjectEmptyException;
import br.com.olympicgames.util.ObjectExistException;
import javax.ejb.embeddable.EJBContainer;
import javax.naming.NamingException;
import org.junit.After;
import org.junit.AfterClass;
import org.junit.Assert;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;

/**
 * Teste para serviço dos jogos.
 *
 * @author filipi
 */
public class GameServiceTest {

    /**
     * Jogo.
     */
    private GameDTO game;

    /**
     * Serviço dos jogos.
     */
    private static GameService gameService;

    /**
     * Container ejb.
     */
    private static EJBContainer container;

    /**
     * Construtor.
     */
    public GameServiceTest() {
    }

    @BeforeClass
    public static void setUpClass() {
        try {
            container = javax.ejb.embeddable.EJBContainer.createEJBContainer();
            gameService = (GameService) container.getContext().lookup("java:global/classes/GameService");
        } catch (final NamingException e) {
            Assert.fail("Falha ao injetar serviço");
        }
    }

    @AfterClass
    public static void tearDownClass() {
        container.close();
    }

    @Before
    public void setUp() {
        game = new GameDTO();
    }

    @After
    public void tearDown() {
    }

    /**
     * Testa método salvar.
     *
     * @throws ObjectEmptyException Jogo em branco
     * @throws FieldInvalidException Campo inválido
     * @throws ObjectExistException Jogo já existe
     * @throws MaxGamesException Limite de jogos atingido
     */
    public void shouldSave() throws ObjectEmptyException, FieldInvalidException,
            ObjectExistException, MaxGamesException {
        game.setFirstCountry("Brasil");
        game.setSecondCountry("Alemanha");
        game.setModality("Futebol");
        game.setLocal("Mineirão");
        game.setInitDate("16-09-2017 08:15:00");
        game.setEndDate("16-09-2017 10:15:00");
        game.setRound("Eliminatórias");

        gameService.save(game);

        Assert.assertEquals(1, gameService.listByModality(null).size());
    }

    /**
     * Testa o método salvar informando os campos de preenchimento obrigatório
     * vazio.
     */
    @Test
    public void shouldNotSaveRequiredNull() {
        try {
            game = null;
            gameService.save(game);
            Assert.fail("Não executou o teste shouldNotSaveRequiredNull com jogo nulo");
        } catch (final Exception e) {
            Assert.assertEquals(Message.GAME_NOT_EMPTY, e.getMessage());
        }
        try {
            game = new GameDTO();
            gameService.save(game);
            Assert.fail("Não executou o teste shouldNotSaveRequiredNull com data inicio nula");
        } catch (final Exception e) {
            Assert.assertEquals(Message.INIT_DATE_NOT_EMPTY, e.getMessage());
        }
        try {
            game.setInitDate("16-09-2017 10:30:00");
            gameService.save(game);
            Assert.fail("Não executou o teste shouldNotSaveRequiredNull com data final nula");
        } catch (final Exception e) {
            Assert.assertEquals(Message.END_DATE_NOT_EMPTY, e.getMessage());
        }
        try {
            game.setEndDate("16-09-2017 11:00:00");
            gameService.save(game);
            Assert.fail("Não executou o teste shouldNotSaveRequiredNull com primeiro pais nulo");
        } catch (final Exception e) {
            Assert.assertEquals(Message.FIRST_COUNTRY_NOT_EMPTY, e.getMessage());
        }
        try {
            game.setFirstCountry("Brasil");
            gameService.save(game);
            Assert.fail("Não executou o teste shouldNotSaveRequiredNull com segundo pais nulo");
        } catch (final Exception e) {
            Assert.assertEquals(Message.SECOND_COUNTRY_NOT_EMPTY, e.getMessage());
        }
        try {
            game.setSecondCountry("Alemanha");
            gameService.save(game);
            Assert.fail("Não executou o teste shouldNotSaveRequiredNull com local nulo");
        } catch (final Exception e) {
            Assert.assertEquals(Message.LOCAL_NOT_EMPTY, e.getMessage());
        }
        try {
            game.setLocal("Mineirão");
            gameService.save(game);
            Assert.fail("Não executou o teste shouldNotSaveRequiredNull com modalidade nula");
        } catch (final Exception e) {
            Assert.assertEquals(Message.MODALITY_NOT_EMPTY, e.getMessage());
        }
        try {
            game.setModality("Basquete");
            gameService.save(game);
            Assert.fail("Não executou o teste shouldNotSaveRequiredNull com etapa nula");
        } catch (final Exception e) {
            Assert.assertEquals(Message.ROUND_NOT_EMPTY, e.getMessage());
        }
        try {
            game.setRound("Eliminatórias");
            gameService.save(game);
        } catch (final Exception e) {
            Assert.fail("Não executou o teste shouldNotSaveRequiredNull com todos campos preenchidos");
        }
    }

    /**
     * Testa o método salvar informando jogo duplicado.
     */
    @Test
    public void shouldNotSaveDuplicated() {
        try {
            game.setFirstCountry("Brasil");
            game.setSecondCountry("Alemanha");
            game.setModality("Futebol");
            game.setLocal("Mineirão");
            game.setInitDate("16-09-2017 08:15:00");
            game.setEndDate("16-09-2017 10:15:00");
            game.setRound("Eliminatórias");

            gameService.save(game);
        } catch (final Exception e) {
            Assert.assertEquals(Message.EXIST_GAME, e.getMessage());
        }
    }

    /**
     * Testa o método salvar informando data inválida.
     */
    @Test
    public void shouldNotSaveInvalidDate() {
        try {
            game.setFirstCountry("Brasil");
            game.setSecondCountry("França");
            game.setModality("Futebol");
            game.setLocal("Maracanã");
            game.setInitDate("16-09-2017");
            game.setEndDate("16-09-2017 12:00:00");
            game.setRound("Eliminatórias");

            gameService.save(game);
            Assert.fail("Não executou o teste shouldNotSaveInvalidDate com data inicio inválida");
        } catch (final Exception e) {
            Assert.assertEquals(Message.INVALID_DATE, e.getMessage());
        }

        try {
            game.setFirstCountry("Brasil");
            game.setSecondCountry("França");
            game.setModality("Futebol");
            game.setLocal("Maracanã");
            game.setInitDate("16-09-2017 11:30:00");
            game.setEndDate("16-09-2017 12:00");
            game.setRound("Eliminatórias");

            gameService.save(game);
            Assert.fail("Não executou o teste shouldNotSaveInvalidDate com data final inválida");
        } catch (final Exception e) {
            Assert.assertEquals(Message.INVALID_DATE, e.getMessage());
        }

        try {
            game.setFirstCountry("Brasil");
            game.setSecondCountry("França");
            game.setModality("Futebol");
            game.setLocal("Maracanã");
            game.setInitDate("16-09-2017 11:30:00");
            game.setEndDate("16-09-2017 10:00:00");
            game.setRound("Eliminatórias");

            gameService.save(game);
            Assert.fail("Não executou o teste shouldNotSaveInvalidDate com periodo inválido");
        } catch (final Exception e) {
            Assert.assertEquals(Message.INVALID_PERIOD, e.getMessage());
        }

        try {
            game.setFirstCountry("Brasil");
            game.setSecondCountry("França");
            game.setModality("Futebol");
            game.setLocal("Maracanã");
            game.setInitDate("16-09-2017 11:30:00");
            game.setEndDate("16-09-2017 11:40:00");
            game.setRound("Eliminatórias");

            gameService.save(game);
            Assert.fail("Não executou o teste shouldNotSaveInvalidDate com periodo do jogo inválido");
        } catch (final Exception e) {
            Assert.assertEquals(Message.GAME_RUNNING_TIME_INVALID, e.getMessage());
        }

        try {
            game.setFirstCountry("Argentina");
            game.setSecondCountry("Holanda");
            game.setModality("Futebol");
            game.setLocal("Maracanã");
            game.setInitDate("16-09-2017 09:30:00");
            game.setEndDate("16-09-2017 12:00:00");
            game.setRound("Eliminatórias");

            gameService.save(game);
        } catch (final Exception e) {
            Assert.fail("Falha ao executar o teste shouldNotSaveInvalidDate com todos campos corretos");
        }

    }

    /**
     * Testa o método salvar informando etapa inválida.
     */
    @Test
    public void shouldNotSaveInvalidRound() {
        try {
            game.setFirstCountry("Brasil");
            game.setSecondCountry("França");
            game.setModality("Futebol");
            game.setLocal("Maracanã");
            game.setInitDate("16-09-2017 13:00:00");
            game.setEndDate("16-09-2017 14:00:00");
            game.setRound("InavlidRound");

            gameService.save(game);
            Assert.fail("Não executou o teste shouldNotSaveInvalidRound com etapa inválida");
        } catch (final Exception e) {
            Assert.assertEquals(Message.INVALID_ROUND, e.getMessage());
        }
    }

    /**
     * Testa método salvar informando país inválido.
     */
    @Test
    public void shouldNotSaveInvalidCountry() {
        try {
            game.setFirstCountry("França");
            game.setSecondCountry("França");
            game.setModality("Handball");
            game.setLocal("Ginásio");
            game.setInitDate("16-09-2017 14:15:00");
            game.setEndDate("16-09-2017 14:45:00");
            game.setRound("Eliminatórias");

            gameService.save(game);
            Assert.fail("Não executou o teste shouldNotSaveInvalidCountry com paises inválidos");
        } catch (final Exception e) {
            Assert.assertEquals(Message.COUNTRY_MUST_BE_DIFFERENT, e.getMessage());
        }

        try {
            game.setFirstCountry("França");
            game.setSecondCountry("França");
            game.setModality("Handball");
            game.setLocal("Ginásio");
            game.setInitDate("16-09-2017 14:15:00");
            game.setEndDate("16-09-2017 14:45:00");
            game.setRound("Final");

            gameService.save(game);
        } catch (final Exception e) {
            Assert.fail("Não executou o teste shouldNotSaveInvalidCountry com paises inválidos");
        }
    }

    /**
     * Testa o método salvar informando quantida inválida de jogos para local.
     */
    @Test
    public void shouldNotSaveMaxGame() {
        try {
            game.setFirstCountry("Japão");
            game.setSecondCountry("China");
            game.setModality("Tênis de Mesa");
            game.setLocal("Ginásio Municipal");
            game.setInitDate("16-09-2017 08:15:00");
            game.setEndDate("16-09-2017 09:45:00");
            game.setRound("Eliminatórias");

            gameService.save(game);
            System.out.println("Criou jogo 1");

            final GameDTO game2 = new GameDTO();
            game2.setFirstCountry("Chile");
            game2.setSecondCountry("Uruguai");
            game2.setModality("Tênis de Mesa");
            game2.setLocal("Ginásio Municipal");
            game2.setInitDate("16-09-2017 09:50:00");
            game2.setEndDate("16-09-2017 10:20:00");
            game2.setRound("Eliminatórias");

            gameService.save(game2);
            System.out.println("Criou jogo 2");

            final GameDTO game3 = new GameDTO();
            game3.setFirstCountry("USA");
            game3.setSecondCountry("Canadá");
            game3.setModality("Tênis de Mesa");
            game3.setLocal("Ginásio Municipal");
            game3.setInitDate("16-09-2017 10:30:00");
            game3.setEndDate("16-09-2017 12:00:00");
            game3.setRound("Eliminatórias");

            gameService.save(game3);
            System.out.println("Criou jogo 3");

            final GameDTO game4 = new GameDTO();
            game4.setFirstCountry("México");
            game4.setSecondCountry("Hungria");
            game4.setModality("Tênis de Mesa");
            game4.setLocal("Ginásio Municipal");
            game4.setInitDate("16-09-2017 13:00:00");
            game4.setEndDate("16-09-2017 14:30:00");
            game4.setRound("Eliminatórias");

            gameService.save(game4);
            System.out.println("Criou jogo 4");

            final GameDTO game5 = new GameDTO();
            game5.setFirstCountry("Espanha");
            game5.setSecondCountry("Italia");
            game5.setModality("Tênis de Mesa");
            game5.setLocal("Ginásio Municipal");
            game5.setInitDate("16-09-2017 14:40:00");
            game5.setEndDate("16-09-2017 15:10:00");
            game5.setRound("Eliminatórias");

            gameService.save(game5);
            Assert.fail("Não executou o teste shouldNotSaveMaxGame com quantidade de jogos inválida");

        } catch (final Exception e) {
            Assert.assertEquals(Message.MAX_GAME, e.getMessage());
        }
    }

    /**
     * Testa método de listagem através da modalidade.
     * 
     * @throws ObjectEmptyException Jogo em branco
     * @throws FieldInvalidException Campo inválido
     * @throws ObjectExistException Jogo já existe
     * @throws MaxGamesException Limite de jogos atingido
     */
    @Test
    public void shouldListByModality() throws ObjectEmptyException, FieldInvalidException, ObjectExistException, MaxGamesException {
        game.setFirstCountry("Japão");
        game.setSecondCountry("China");
        game.setModality("Tênis");
        game.setLocal("Arena");
        game.setInitDate("16-09-2017 18:15:00");
        game.setEndDate("16-09-2017 21:45:00");
        game.setRound("Eliminatórias");

        gameService.save(game);
        
        Assert.assertEquals(1, gameService.listByModality("Tênis").size());
    }
}
