/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.olympicgames.util;

/**
 * Exceção para transportar valores referente a campos inválidos.
 *
 * @author filipi
 */
public class FieldInvalidException extends Exception {

    /**
     * Construtor.
     * 
     * @param message mensagem de exceção a ser setada
     */
    public FieldInvalidException(final String message) {
        super(message);
    }
    
    
    
}
