/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.olympicgames.util;

import java.util.ResourceBundle;

/**
 * Constante de mensagens da camada EJB.
 *
 * @author filipi
 */
public class Message {

    /**
     * Bundle de mensagem.
     */
    private static final ResourceBundle BUNDLE = ResourceBundle.getBundle("bundle");

    /**
     * Mensagem de registro que não pode ser em branco.
     */
    public static final String NOT_EMPTY = " " + BUNDLE.getString("notEmpty");

    /**
     * Mensagem de jogo não pode ser em vazio.
     */
    public static final String GAME_NOT_EMPTY = BUNDLE.getString("game") + NOT_EMPTY;

    /**
     * Mensagem de data não pode ser em vazia.
     */
    public static final String INIT_DATE_NOT_EMPTY = BUNDLE.getString("init") + NOT_EMPTY;

    /**
     * Mensagem de data não pode ser em vazia.
     */
    public static final String END_DATE_NOT_EMPTY = BUNDLE.getString("end") + NOT_EMPTY;

    /**
     * Mensagem de data inválida.
     */
    public static final String INVALID_DATE = BUNDLE.getString("invalidDate");

    /**
     * Mensagem de etapa inválida.
     */
    public static final String INVALID_ROUND = BUNDLE.getString("invalidRound");

    /**
     * Mensagem de etapa que não pode ser em vazia.
     */
    public static final String ROUND_NOT_EMPTY = BUNDLE.getString("round") + NOT_EMPTY;

    /**
     * Mensagem de local não pode ser em vazio.
     */
    public static final String LOCAL_NOT_EMPTY = BUNDLE.getString("local") + NOT_EMPTY;

    /**
     * Mensagem de modalidade não pode ser em vazio.
     */
    public static final String MODALITY_NOT_EMPTY = BUNDLE.getString("modality") + NOT_EMPTY;

    /**
     * Mensagem de primeiro país não pode ser em vazio.
     */
    public static final String FIRST_COUNTRY_NOT_EMPTY = BUNDLE.getString("firstCountry") + NOT_EMPTY;

    /**
     * Mensagem de segundo país não pode ser em vazio.
     */
    public static final String SECOND_COUNTRY_NOT_EMPTY = BUNDLE.getString("secondCountry") + NOT_EMPTY;
    
    /**
     * Mensagem de período inválido.
     */
    public static final String INVALID_PERIOD = BUNDLE.getString("endDateMustBeGreaterThanInitDate");
    
    /**
     * Mensagem de tempo de jogo inválido.
     */
    public static final String GAME_RUNNING_TIME_INVALID = BUNDLE.getString("gameRunningTimeInvalid");
    
    /**
     * Mensagem de paises invalidos.
     */
    public static final String COUNTRY_MUST_BE_DIFFERENT = BUNDLE.getString("countryMustBeDifferent");
    
    /**
     * Mensagem de jogo existente.
     */
    public static final String EXIST_GAME = BUNDLE.getString("existGame");
    
        /**
     * Mensagem de jogo existente.
     */
    public static final String MAX_GAME = BUNDLE.getString("maxGame");
}
