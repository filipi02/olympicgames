/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.olympicgames.util;

/**
 * Exceção para transportar valores referente a objeto existente.
 * 
 * @author filipi
 */
public class ObjectExistException extends Exception {

    /**
     * Construtor.
     * 
     * @param message Mensagem a ser setada
     */
    public ObjectExistException(String message) {
        super(message);
    }
    
}
