/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.olympicgames.util;

/**
 * Exceção para transportar valores referente a objeto vazio.
 * 
 * @author filipi
 */
public class ObjectEmptyException extends Exception {

    /**
     * Construtor.
     * 
     * @param message mensagem de exceção a ser setada
     */
    public ObjectEmptyException(String message) {
        super(message);
    }
    
}
