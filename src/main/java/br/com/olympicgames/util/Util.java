/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.olympicgames.util;

import br.com.olympicgames.model.GameRound;
import java.text.ParseException;
import java.text.SimpleDateFormat;
import java.time.Instant;
import java.time.LocalDateTime;
import java.time.ZoneId;
import java.util.Date;
import org.joda.time.DateTime;

/**
 * Classe utilitária.
 *
 * @author filipi
 */
public final class Util {

    /**
     * Converte data string para datetime.
     *
     * @param string Data a ser convertida
     *
     * @return Data convertida
     *
     * @throws FieldInvalidException Data inválida
     */
    public static Date convertStringToDateTime(final String string) throws FieldInvalidException {
        try {
            SimpleDateFormat sdf = new SimpleDateFormat("dd-MM-yyyy HH:mm:ss");
            return sdf.parse(string);
        } catch (final ParseException e) {
            throw new FieldInvalidException(Message.INVALID_DATE);
        }
    }

    /**
     * Converte etapa string para enum.
     *
     * @param string Etapa a ser convertida
     *
     * @return Etapa convertida
     *
     * @throws FieldInvalidException Campo inválido
     */
    public static GameRound convertStringToGameRound(final String string) throws FieldInvalidException {
        final GameRound gameRound = GameRound.fromString(string);
        if (gameRound != null) {
            return gameRound;
        } else {
            throw new FieldInvalidException(Message.INVALID_ROUND);
        }
    }
    
    /**
     * Converte date em datetime.
     *
     * @param date Data a ser convertida
     *
     * @return Data convertida
     */
    public static DateTime convertDateToDateTime(final Date date) {
        return new DateTime(date);
    }

    /**
     * Converte date em localDateTime.
     *
     * @param date Data a ser convertida
     *
     * @return Data convertida
     */
    public static LocalDateTime convertDateToLocalDateTime(final Date date) {
        Instant instant = Instant.ofEpochMilli(date.getTime());
        return LocalDateTime.ofInstant(instant, ZoneId.systemDefault());
    }
}
