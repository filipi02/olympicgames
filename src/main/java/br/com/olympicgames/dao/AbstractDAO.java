/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.olympicgames.dao;

import java.util.List;
import javax.persistence.EntityManager;
import javax.persistence.PersistenceContext;

/**
 * Classe genérica para persistência.
 * 
 * @author filipi
 */
public class AbstractDAO<T> {
    
    /**
     * Objeto para persistência.
     */
    @PersistenceContext(unitName = "br.com_olympicgames_war_1.0-SNAPSHOTPU")
    private EntityManager em;
    
    /**
     * Classe do tipo {@link T}
     */
    private Class<T> entity;
    
    /**
     * Construtor.
     * 
     * @param entity Entidade à ser setada
     */
    public AbstractDAO(Class<T> entity) {
        this.entity = entity;
    }
    
    /**
     * Salva um objeto.
     * 
     * @param entity Obejto à ser salvo
     */
    public void save(final T entity) {
        em.persist(entity);
    }
    
    /**
     * @return Objeto para persistência
     */
    public EntityManager getEm() {
        return em;
    }
}
