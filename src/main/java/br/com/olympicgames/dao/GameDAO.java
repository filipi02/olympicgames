/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.olympicgames.dao;

import br.com.olympicgames.model.Game;
import br.com.olympicgames.util.Util;
import java.time.LocalDate;
import java.time.ZoneId;
import java.util.Date;
import java.util.List;
import javax.ejb.Stateless;
import javax.persistence.Query;
import org.joda.time.DateTime;
import org.joda.time.DateTimeZone;

/**
 * Classe para manipulação de dados dos jogos.
 *
 * @author filipi
 */
@Stateless
public class GameDAO extends AbstractDAO<Game> {

    /**
     * Construtor.
     */
    public GameDAO() {
        super(Game.class);
    }

    /**
     * Lista de todos os jogos cadastrados através da modalidade.
     *
     * @param modality Modalidade a ser filtrada
     * 
     * @return Lista do tipo {@link List} dos jogos do tipo {@link Game}
     * cadastrados
     */
    public List<Game> listByModality(final String modality) {
        final StringBuilder sql = new StringBuilder();
        sql.append("SELECT game "
                 + "  FROM Game game");
        if (modality != null && modality != "") {
            sql.append(" WHERE game.modality = :modality ");
        }
        sql.append(" ORDER BY game.initDate");
        
        final Query query = getEm().createQuery(sql.toString());
        if (modality != null && modality != "") {
            query.setParameter("modality", modality);
        }
        
        return query.getResultList();
    }

    /**
     * Verifica se existe jogo.
     *
     * @param game Jogo a ser verificado
     *
     * @return True se existir jogo cadastrado
     */
    public boolean checkGameExist(final Game game) {
        final String sql = "SELECT game "
                + "  FROM Game game "
                + " WHERE game.modality = :modality "
                + "   AND game.local = :local "
                + "   AND (game.initDate BETWEEN :initDate AND :endDate "
                + "    OR game.endDate BETWEEN :initDate AND :endDate)";

        final Query query = getEm().createQuery(sql);
        query.setParameter("modality", game.getModality());
        query.setParameter("local", game.getLocal());
        query.setParameter("initDate", game.getInitDate());
        query.setParameter("endDate", game.getEndDate());

        final List<Game> games = query.getResultList();
        return !games.isEmpty();
    }

    /**
     * Verifica se atingiu quantidade máxima de jogos para local.
     *
     * @param game Jogo a ser verificado
     *
     * @return True se atingiu quantidade máxima de jogos para local
     */
    public boolean checkMaxGames(final Game game) {
        final DateTime dateTime = Util.convertDateToDateTime(game.getInitDate());
        final Date init = dateTime.withTimeAtStartOfDay().toDate();
        final Date end = dateTime.plusDays(1).withTimeAtStartOfDay().toDate();
        final String sql = "SELECT game "
                + "  FROM Game game "
                + " WHERE game.modality = :modality "
                + "   AND game.local = :local "
                + "   AND game.initDate BETWEEN :initDate AND :endDate";

        final Query query = getEm().createQuery(sql);
        query.setParameter("modality", game.getModality());
        query.setParameter("local", game.getLocal());
        query.setParameter("initDate", init);
        query.setParameter("endDate", end);

        final List<Game> games = query.getResultList();
        return games.size() >= 4;
    }

}
