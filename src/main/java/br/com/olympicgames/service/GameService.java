/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.olympicgames.service;

import br.com.olympicgames.dao.GameDAO;
import br.com.olympicgames.model.Game;
import br.com.olympicgames.model.GameDTO;
import br.com.olympicgames.model.GameRound;
import br.com.olympicgames.util.FieldInvalidException;
import br.com.olympicgames.util.MaxGamesException;
import br.com.olympicgames.util.Message;
import br.com.olympicgames.util.ObjectEmptyException;
import br.com.olympicgames.util.ObjectExistException;
import br.com.olympicgames.util.Util;
import java.time.LocalDateTime;
import java.util.Date;
import java.util.List;
import javax.ejb.EJB;
import javax.ejb.Stateless;

/**
 * Classe para serviço dos jogos.
 *
 * @author filipi
 */
@Stateless
public class GameService {

    /**
     * Injeção para manipulação de dados dos jogos.
     */
    @EJB
    private GameDAO gameDAO;

    /**
     * Cria novo jogo.
     *
     * @param gameDTO Jogo a ser criado
     *
     * @throws ObjectEmptyException Objeto em branco
     * @throws FieldInvalidException Campo inválido
     * @throws ObjectExistException Objeto já existe
     * @throws MaxGamesException Máximo de jogo atingidos
     */
    public void save(final GameDTO gameDTO) throws ObjectEmptyException,
            FieldInvalidException, ObjectExistException, MaxGamesException {
        this.checkRequired(gameDTO);
        final Game game = this.convertGame(gameDTO);
        this.checkDate(game);
        this.checkCountry(game);
        this.checkGameExist(game);
        this.checkMaxGames(game);
        gameDAO.save(game);
    }

    /**
     * Lista de todos os jogos cadastrados através da modalidade.
     *
     * @param modality Modalidade a ser filtrada
     *
     * @return Lista do tipo {@link List} dos jogos do tipo {@link Game}
     * cadastrados
     */
    public List<Game> listByModality(final String modality) {
        return gameDAO.listByModality(modality);
    }

    /**
     * Verifica preenchimento dos campos obrigatórios.
     *
     * @param game Jogo do tipo {@link Game} a ser verificado
     *
     * @throws ObjectEmptyException Objeto em branco
     * @throws FieldInvalidException Campo inválido
     */
    private void checkRequired(final GameDTO gameDTO) throws ObjectEmptyException, FieldInvalidException {
        if (gameDTO == null) {
            throw new ObjectEmptyException(Message.GAME_NOT_EMPTY);
        }

        if (gameDTO.getInitDate() == null) {
            throw new FieldInvalidException(Message.INIT_DATE_NOT_EMPTY);
        }

        if (gameDTO.getEndDate() == null) {
            throw new FieldInvalidException(Message.END_DATE_NOT_EMPTY);
        }

        if (gameDTO.getFirstCountry() == null) {
            throw new FieldInvalidException(Message.FIRST_COUNTRY_NOT_EMPTY);
        }

        if (gameDTO.getSecondCountry() == null) {
            throw new FieldInvalidException(Message.SECOND_COUNTRY_NOT_EMPTY);
        }

        if (gameDTO.getLocal() == null) {
            throw new FieldInvalidException(Message.LOCAL_NOT_EMPTY);
        }

        if (gameDTO.getModality() == null) {
            throw new FieldInvalidException(Message.MODALITY_NOT_EMPTY);
        }

        if (gameDTO.getRound() == null) {
            throw new FieldInvalidException(Message.ROUND_NOT_EMPTY);
        }
    }

    /**
     * Converte dto do jogo para entidade a ser persistida.
     *
     * @param gameDTO Dto do jogo a ser convertido
     *
     * @return Jogo convertido
     *
     * @throws FieldInvalidException Campo inválido
     */
    private Game convertGame(final GameDTO gameDTO) throws FieldInvalidException {
        final Date init = Util.convertStringToDateTime(gameDTO.getInitDate());
        final Date end = Util.convertStringToDateTime(gameDTO.getEndDate());
        final GameRound gameRound = Util.convertStringToGameRound(gameDTO.getRound());
        return new Game(gameDTO.getModality(), gameDTO.getLocal(), init, end,
                gameDTO.getFirstCountry(), gameDTO.getSecondCountry(), gameRound);
    }

    /**
     * Verifica se existe jogo.
     *
     * @param game Jogo a ser verificado
     *
     * @return True se existir jogo cadastrado
     *
     * @throws ObjectExistException Jogo já existe
     */
    private boolean checkGameExist(final Game game) throws ObjectExistException {
        final boolean existGame = gameDAO.checkGameExist(game);
        if (existGame) {
            throw new ObjectExistException(Message.EXIST_GAME);
        }

        return existGame;
    }

    /**
     * Verifica se atingiu quantidade máxima de jogos para local.
     *
     * @param game Jogo a ser verificado
     *
     * @return True se atingiu quantidade máxima de jogos para local
     *
     * @throws MaxGamesException Quantidade máxima de jogos atingida
     */
    public boolean checkMaxGames(final Game game) throws MaxGamesException {
        final boolean maxGame = gameDAO.checkMaxGames(game);
        if (maxGame) {
            throw new MaxGamesException(Message.MAX_GAME);
        }

        return maxGame;
    }

    /**
     * Verifica as data definidas para o jogo.
     *
     * @param game Jogo a ser verificado
     *
     * @throws FieldInvalidException Data inválida
     */
    private void checkDate(final Game game) throws FieldInvalidException {
        if (!game.isInitBeforeEnd()) {
            throw new FieldInvalidException(Message.INVALID_PERIOD);
        } else if (game.isInvalidPeriod()) {
            throw new FieldInvalidException(Message.GAME_RUNNING_TIME_INVALID);
        }
    }

    /**
     * Verifica paises do jogo.
     *
     * @param game Jogo a ser verificado
     *
     * @throws FieldInvalidException Paises inválidos
     */
    private void checkCountry(final Game game) throws FieldInvalidException {
        if (game.isCountriesEquals() && !game.isSemiFinal() && !game.isFinal()) {
            throw new FieldInvalidException(Message.COUNTRY_MUST_BE_DIFFERENT);
        }
    }
}
