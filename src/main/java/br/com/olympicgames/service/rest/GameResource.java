/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.olympicgames.service.rest;

import br.com.olympicgames.model.GameDTO;
import br.com.olympicgames.service.GameService;
import br.com.olympicgames.util.FieldInvalidException;
import br.com.olympicgames.util.MaxGamesException;
import br.com.olympicgames.util.ObjectEmptyException;
import br.com.olympicgames.util.ObjectExistException;
import com.google.gson.Gson;
import javax.ejb.EJB;
import javax.ws.rs.Consumes;
import javax.ws.rs.Path;
import javax.ws.rs.GET;
import javax.ws.rs.POST;
import javax.ws.rs.PathParam;
import javax.ws.rs.Produces;
import javax.ws.rs.core.Response;

/**
 * Web Service para jogos.
 *
 * @author filipi
 */
@Path("game")
public class GameResource {

    /**
     * Injeção para serviço dos jogos.
     */
    @EJB
    private GameService gameService;

    /**
     * Construtor.
     */
    public GameResource() {
    }

    /**
     * Cria novo jogo.
     *
     * @param gameDTO Objeto que transporta valores do jogo a ser criado
     *
     * @return Jogo criado
         */
    @POST
    @Produces("application/json")
    @Consumes("application/json")
    public Response addGame(final GameDTO gameDTO) {
        try {
            gameService.save(gameDTO);
            return Response.ok("Jogo criado com sucesso").build();
        } catch (final ObjectEmptyException | FieldInvalidException | 
                ObjectExistException | MaxGamesException e) {
            final Gson gson = new Gson();
            return Response.status(Response.Status.PRECONDITION_FAILED).
                    entity(gson.toJson(e)).build();
        }
    }

    /**
     * Lista os jogos criados.
     *
     * @return Jogos criados
     */
    @GET
    @Produces("application/json")
    public Response list() {
        final Gson gson = new Gson();
        return Response.ok(gson.toJson(gameService.listByModality(null))).build();
    }
    
    /**
     * Lista os jogos criados através da modalidade.
     *
     * @return Jogos criados
     */
    @GET
    @Produces("application/json")
    @Path("/{modality}")
    public Response listByModality(@PathParam("modality") String modality) {
        final Gson gson = new Gson();
        return Response.ok(gson.toJson(gameService.listByModality(modality))).build();
    }

}
