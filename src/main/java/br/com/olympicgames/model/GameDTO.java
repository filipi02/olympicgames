/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.olympicgames.model;

import java.util.Date;

/**
 *
 * @author filipi
 */
public class GameDTO {

    /**
     * Modalidade.
     */
    private String modality;

    /**
     * Local.
     */
    private String local;

    /**
     * Data inicio.
     */
    private String initDate;

    /**
     * Data fim.
     */
    private String endDate;

    /**
     * Pais A.
     */
    private String firstCountry;

    /**
     * Pais B.
     */
    private String secondCountry;

    /**
     * Etapa.
     */
    private String round;

    /**
     * @return valor do modality
     */
    public String getModality() {
        return modality;
    }

    /**
     * @param modality Valor a ser setado no modality
     */
    public void setModality(String modality) {
        this.modality = modality;
    }

    /**
     * @return valor do local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param local Valor a ser setado no local
     */
    public void setLocal(String local) {
        this.local = local;
    }

     /**
     * @return valor do initDate
     */
    public String getInitDate() {
        return initDate;
    }
    
     /**
     * @param initDate valor a ser setado no initDate
     */
    public void setInitDate(final String initDate) { 
        this.initDate = initDate;
    }

    /**
     * @return valor do endDate
     */
    public String getEndDate() {
        return endDate;
    }

    /**
     * @param endDate valor a ser setado no endDate
     */
    public void setEndDate(String endDate) {
        this.endDate = endDate;
    }
    
    /**
     * @return valor do firstCountry
     */
    public String getFirstCountry() {
        return firstCountry;
    }

    /**
     * @param firstCountry valor a ser setado no firstCountry
     */
    public void setFirstCountry(String firstCountry) {
        this.firstCountry = firstCountry;
    }

    /**
     * @return valor do secondCountry
     */
    public String getSecondCountry() {
        return secondCountry;
    }

    /**
     * @param secondCountry valor a ser setado no secondCountry
     */
    public void setSecondCountry(String secondCountry) {
        this.secondCountry = secondCountry;
    }

    /**
     * @return valor do round
     */
    public String getRound() {
        return round;
    }

    /**
     * @param round valor a ser setado no round
     */
    public void setRound(String round) {
        this.round = round;
    }
}
