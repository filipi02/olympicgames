/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.olympicgames.model;

import br.com.olympicgames.util.Util;
import java.io.Serializable;
import java.util.Date;
import javax.persistence.Basic;
import javax.persistence.Column;
import javax.persistence.Entity;
import javax.persistence.EnumType;
import javax.persistence.Enumerated;
import javax.persistence.GeneratedValue;
import javax.persistence.GenerationType;
import javax.persistence.Id;
import javax.persistence.NamedQueries;
import javax.persistence.NamedQuery;
import javax.persistence.Table;
import javax.persistence.Temporal;
import javax.persistence.TemporalType;

/**
 * Entidade responsável pelos jogos da olimpíada.
 *
 * @author filipi
 */
@Entity
@Table(name = "game")
public class Game implements Serializable {

    /**
     * Serial version
     */
    private static final long serialVersionUID = 1L;

    /**
     * Identificador.
     */
    @Id
    @GeneratedValue(strategy = GenerationType.IDENTITY)
    @Basic(optional = false)
    @Column(name = "id")
    private long id;

    /**
     * Modalidade.
     */
    @Basic(optional = false)
    @Column(name = "modality")
    private String modality;

    /**
     * Local.
     */
    @Basic(optional = false)
    @Column(name = "local")
    private String local;

    /**
     * Data inicio.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Basic(optional = false)
    @Column(name = "init_date")
    private Date initDate;

    /**
     * Data fim.
     */
    @Temporal(TemporalType.TIMESTAMP)
    @Basic(optional = false)
    @Column(name = "end_date")
    private Date endDate;

    /**
     * Pais A.
     */
    @Basic(optional = false)
    @Column(name = "first_country")
    private String firstCountry;

    /**
     * Pais B.
     */
    @Basic(optional = false)
    @Column(name = "second_country")
    private String secondCountry;

    /**
     * Etapa.
     */
    @Enumerated(EnumType.STRING)
    @Basic(optional = false)
    @Column(name = "round", length = 16)
    private GameRound round;

    /**
     * Construtor.
     */
    public Game() {
    }

    /**
     * Construtor.
     *
     * @param modality Modalidade a ser setada
     * @param local Local a ser setado
     * @param init Data inicio a ser setada
     * @param end Data final a ser setada
     * @param firstCountry País a ser setado
     * @param secondCountry País a ser setado
     * @param round Etapa a ser setada
     */
    public Game(String modality, String local, Date init, Date end, String firstCountry, String secondCountry, GameRound round) {
        this.modality = modality;
        this.local = local;
        this.initDate = init;
        this.endDate = end;
        this.firstCountry = firstCountry;
        this.secondCountry = secondCountry;
        this.round = round;
    }
    
    /**
     * Verifica se data inicio é maior que data fim.
     * 
     * @return True se data inicio é maior que data fim
     */
    public boolean isInitBeforeEnd() {
        return Util.convertDateToDateTime(this.getInitDate()).isBefore(Util.convertDateToDateTime(this.getEndDate()));
    }
    
    /**
     * Verifica se duração do jogo é inválida.
     * 
     * @return True se duração do jogo é inválida
     */
    public boolean isInvalidPeriod() {
        return Util.convertDateToDateTime(this.getEndDate()).minusMinutes(30).isBefore(Util.convertDateToDateTime(this.getInitDate()));
    }
    
    /**
     * Verifica se paises são iguais.
     *
     * @return True se paises forem iguais
     */
    public boolean isCountriesEquals() {
        return firstCountry.equals(secondCountry);
    }

    /**
     * Verifica se jogo é semifinal.
     *
     * @return True se jogo for semifinal
     */
    public boolean isSemiFinal() {
        return round == GameRound.SEMI_FINAL;
    }

    /**
     * Verifica se jogo é final.
     *
     * @return True se jogo for final
     */
    public boolean isFinal() {
        return round == GameRound.FINAL;
    }

    /**
     * @return valor do id
     */
    public long getId() {
        return id;
    }

    /**
     * @param id Valor a ser setado no id
     */
    public void setId(long id) {
        this.id = id;
    }

    /**
     * @return valor do modality
     */
    public String getModality() {
        return modality;
    }

    /**
     * @param modality Valor a ser setado no modality
     */
    public void setModality(String modality) {
        this.modality = modality;
    }

    /**
     * @return valor do local
     */
    public String getLocal() {
        return local;
    }

    /**
     * @param local Valor a ser setado no local
     */
    public void setLocal(String local) {
        this.local = local;
    }

    /**
     * @return valor do initDate
     */
    public Date getInitDate() {
        return initDate;
    }

    /**
     * @param initDate valor a ser setado no initDate
     */
    public void setInitDate(final Date initDate) {
        this.initDate = initDate;
    }

    /**
     * @return valor do endDate
     */
    public Date getEndDate() {
        return endDate;
    }

    /**
     * @param endDate valor a ser setado no endDate
     */
    public void setEndDate(Date endDate) {
        this.endDate = endDate;
    }

    /**
     * @return valor do firstCountry
     */
    public String getFirstCountry() {
        return firstCountry;
    }

    /**
     * @param firstCountry valor a ser setado no firstCountry
     */
    public void setFirstCountry(String firstCountry) {
        this.firstCountry = firstCountry;
    }

    /**
     * @return valor do secondCountry
     */
    public String getSecondCountry() {
        return secondCountry;
    }

    /**
     * @param secondCountry valor a ser setado no secondCountry
     */
    public void setSecondCountry(String secondCountry) {
        this.secondCountry = secondCountry;
    }

    /**
     * @return valor do round
     */
    public GameRound getRound() {
        return round;
    }

    /**
     * @param round valor a ser setado no round
     */
    public void setRound(GameRound round) {
        this.round = round;
    }

}
