/*
 * To change this license header, choose License Headers in Project Properties.
 * To change this template file, choose Tools | Templates
 * and open the template in the editor.
 */
package br.com.olympicgames.model;

/**
 * Enum para etapa dos jogos.
 *
 * @author filipi
 */
public enum GameRound {

    ELIMINATORIAS("Eliminatórias"),
    OITAVAS_DE_FINAL("Oitavas de final"),
    QUARTAS_DE_FINAL("Quartas de final"),
    SEMI_FINAL("Semifinal"),
    FINAL("Final");

    /**
     * Valor enum.
     */
    private String value;

    GameRound(final String value) {
        this.value = value;
    }
    
    /**
     * Recupera enum através do valor.
     * 
     * @param text valor do enum a ser recuperado
     * 
     * @return Enum recuperado
     */
    public static GameRound fromString(String text) {
    for (GameRound gameRound : GameRound.values()) {
      if (gameRound.value.equalsIgnoreCase(text)) {
        return gameRound;
      }
    }
    return null;
  }
}
